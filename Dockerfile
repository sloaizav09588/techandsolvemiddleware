FROM openjdk:8

COPY ./s-1.0.jar /app.jar
COPY ./run.sh /run.sh
COPY ./application.yaml /application.yaml
RUN chmod +x /run.sh

EXPOSE 9000
ENTRYPOINT ["/run.sh"]
